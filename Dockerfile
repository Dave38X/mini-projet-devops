FROM php:apache
RUN apt update
COPY www/insert.php /var/www/html/insert.php
RUN docker-php-ext-install mysqli
RUN docker-php-ext-enable mysqli
RUN apachectl restart
COPY www/index.php /var/www/html/index.php

