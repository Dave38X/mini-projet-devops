Mini projet devops

Ce docker permet de créer et d'afficher une liste de course avec stockage des information en base de donnée

# Installation

Pour installer le docker, utilisez la commande suivante:

git clone git@gitlab.com:Dave38X/mini-projet-devops.git

Il comprend deux services, un front end basé sur l'image php:apache et un backend basé sur l'image mariadb.

Après avoir modifié les variables d'environnement contenues dans le .env.sample, le renomer en .env et lancer la commande "docker-compose up --build"
Veuillez attendre une dizaine de secondes avant d'utiliser le site : la base de données s'initialise, ce qui rend le site non fonctionnel durant cette intervalle.

# Descriptif du contenu

## fichiers

".env.sample" A modifier ! Il contient les variables d'environnement qui permettent de customiser le docker

### Variables modifiables

- NOM > Nom affiché par l'application sur la page d'accueil
- MYSQL_DATABASE > Nom de la base de données de l'application
- MYSQL_USER > Utilisateur de la base donnée
- MYSQL_PASSWORD > Mot de passe de l'utilisateur de la base de donnée
- MYSQL_ROOT_PASSWORD > Mot de passe du super admin de la base de donnée
- APACHE_PORT > Permet de modifier le port d'écoute sur lequel l'application sera disponible

"DockerFile" Il contient la création de l'image front

"docker-compose.yml" Il contient

## Dossiers

- Un dossier data permettant la persistance des données avec la base de donnée.
    C'est dans ce dossier que pourront être récupérée les données du docker
- Un dossier init permettant d'initialiser les tables de la base de donnée
- Un dossier www contenant les fichiers de l'application servi par le front end.

## notes sur le fonctionnement du site
Après avoir ajouté votre entrée, un message s'affichera pour signaler que votre entrée a bien été ajoutée dans la base de données.
Deux secondes plus tard, vous serez redirigé sur le site, avec la liste actualisée !!
C'est merveilleux n'est ce pas 

